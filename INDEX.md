# PRINT

Print files in the background while you do other things.


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## PRINT.LSM

<table>
<tr><td>title</td><td>PRINT</td></tr>
<tr><td>version</td><td>1.02.ea (rev A)</td></tr>
<tr><td>entered&nbsp;date</td><td>2008-12-12</td></tr>
<tr><td>description</td><td>Print files in the background while you do other things.</td></tr>
<tr><td>keywords</td><td>print, spool, lpr</td></tr>
<tr><td>author</td><td>James Tabor &lt;jimtabor -AT- infohwy.com&gt;</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Print</td></tr>
</table>
